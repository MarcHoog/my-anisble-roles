terraform {
  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = "2.9.10"
    }
  }

  backend "remote" {
  organization = "zamyza"
  workspaces {
    name = "my_ansible_roles_lab"
    }
  }

}

provider "proxmox" {
  pm_api_url = "https://10.0.0.253:8006/api2/json"
  pm_api_token_id = var.proxmox_api_user
  pm_api_token_secret =  var.proxmox_api_key # THIS is in a local closed of test environment
  pm_tls_insecure = true
}


resource "proxmox_vm_qemu" "ansible-demo" {
    name        = "ansible-demo"
    agent       = 1
    target_node = "ml350p"
    onboot      = false 
    qemu_os     = "l26"
    clone       = "SRV-Ubuntu-Focal"
    full_clone  = false

    // CPU
    sockets = 2
    cores   = 4
    memory  = 4096
    
    // CLOUD-INIT
    ipconfig0     = "ip=10.0.0.20/24,gw=10.0.0.254"
    ciuser        = "bubble"
    sshkeys       = var.ansible_ssh_key
    searchdomain  = "mylab.local"
    nameserver    = "10.0.0.254"

    network {
        bridge      = "vmbr120"
        firewall    = false
        link_down   = false
        model       = "virtio"
    }

    disk {
    //    id              = 0
        type            = "scsi"
        storage         = "local-lvm"
        size            = "32972M"
        backup          = 0
    }
}
