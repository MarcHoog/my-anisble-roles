variable "proxmox_api_user" {
    type        = string
    description = "This is an example input variable using env variables."
}
variable "proxmox_api_key" {
    type        = string
    description = "This is another example input variable using env variables."
}
variable "ansible_ssh_key" {
    type        = string
    description = "This is another example input variable using env variables."
}
